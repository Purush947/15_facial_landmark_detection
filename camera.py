import cv2
import time 
from keras.models import load_model
import numpy as np
from utils import *

# Load in color image for face detection
image = cv2.imread('images/obamas4.jpg')
model = load_model('my_model.h5')

# Convert the image to RGB colorspace
image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)


def face_keypoint_detector(image):
    '''
        Takes in an image(BGR) and plots the facial bounding box and keypoints on the image.
        
        Returns the new image, the face bounding box coordinates and the keypoint coordinates.
    '''
    # Convert image to grayscale
    image_copy = np.copy(image)
    gray = cv2.cvtColor(image_copy, cv2.COLOR_BGR2GRAY)

    # Detect faces
    face_cascade = cv2.CascadeClassifier('detector_architectures/haarcascade_frontalface_default.xml')
    faces = face_cascade.detectMultiScale(gray, 2.75,8)
    faces_keypoints = []
    print(faces)






    for i, (x,y,w,h) in enumerate(faces):
        print(i)
        cv2.rectangle(image_copy, (x,y), (x+w,y+h),(255,120,150), 3)
        # Crop Faces
        face = gray[y:y+h, x:x+w]

        # Scale Faces to 96x96
        scaled_face = cv2.resize(face, (96,96), 0, 0, interpolation=cv2.INTER_AREA)

        # Normalize images to be between 0 and 1
        input_image = scaled_face / 255

        # Format image to be the correct shape for the model
        input_image = np.expand_dims(input_image, axis = 0)
        input_image = np.expand_dims(input_image, axis = -1)

        # Use model to predict keypoints on image
        landmarks = model.predict(input_image)[0]
        # Adjust keypoints to coordinates of original image
        landmarks[0::2] = landmarks[0::2] * w/2 + w/2 + x
        landmarks[1::2] = landmarks[1::2] * h/2 + h/2 + y
        faces_keypoints.append(landmarks)
        
        # Paint keypoints on image
        for point in range(15):
            cv2.circle(image_copy, (landmarks[2*point], landmarks[2*point + 1]), 2, (255, 255, 0), -1)
        
    return image_copy, faces, faces_keypoints

img, faces, keypoints = face_keypoint_detector(image)
img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
fig = plt.figure(figsize=(20,20))
ax = fig.add_subplot(111)
ax.set_xticks([])
ax.set_yticks([])
ax.imshow(img)
cv2.imshow('image',img)
cv2.waitKey(0)
cv2.destroyAllWindows()

def laptop_camera_go():
    # Create instance of video capturer
    cv2.namedWindow("face detection activated")
    vc = cv2.VideoCapture(0)

    # try to get the first frame
    if vc.isOpened(): 
        rval, frame = vc.read()
    else:
        rval = False
    
    # Keep video stream open
    while rval:
        _, _, keypoints = face_keypoint_detector(frame)
        if len(keypoints) > 0:
            frame = overlay_sunglasses(keypoints, frame)
            
        # Plot image from camera with detections marked
        cv2.imshow("face detection activated", frame)
        
        # Exit functionality - press any key to exit laptop video
        key = cv2.waitKey(20)
        if key > 0: # exit by pressing any key
            # Destroy windows 
            cv2.destroyAllWindows()
            
            for i in range (1,5):
                cv2.waitKey(1)
            return
        
        # Read next frame
        time.sleep(0.05)             # control framerate for computation - default 20 frames per sec
        rval, frame = vc.read()



# laptop_camera_go()
