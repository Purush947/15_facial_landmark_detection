import cv2
import matplotlib.pyplot as plt
import numpy as np
from keras.models import load_model


# Load in color image for face detection
image = cv2.imread('images/Arun_1.jpg')
model = load_model('With_original.h5')

# Convert the image to RGB colorspace
image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
### TODO: Use the face detection code we saw in Section 1 with your trained conv-net
def face_keypoint_detector(image):
    '''
        Takes in an image(BGR) and plots the facial bounding box and keypoints on the image.
        
        Returns the new image, the face bounding box coordinates and the keypoint coordinates.
    '''
    # Convert image to grayscale
    image_copy = np.copy(image)
    gray = cv2.cvtColor(image_copy, cv2.COLOR_BGR2GRAY)
    # Detect faces
    face_cascade = cv2.CascadeClassifier('detector_architectures/haarcascade_frontalface_default.xml')
    faces = face_cascade.detectMultiScale(gray, 2.75,8)
    faces_keypoints = []
    print(faces)
    
    # Loop through faces
    for i, (x,y,w,h) in enumerate(faces):
        cv2.rectangle(image_copy, (x,y), (x+w,y+h),(255,120,150), 3)
        # Crop Faces
        face = gray[y:y+h, x:x+w]

        # Scale Faces to 96x96
        scaled_face = cv2.resize(face, (96,96), 0, 0, interpolation=cv2.INTER_AREA)

        # Normalize images to be between 0 and 1
        input_image = scaled_face / 255

        # Format image to be the correct shape for the model
        input_image = np.expand_dims(input_image, axis = 0)
        input_image = np.expand_dims(input_image, axis = -1)

        # Use model to predict keypoints on image
        landmarks = model.predict(input_image)[0]
        print(landmarks)
        print("change")
        # Adjust keypoints to coordinates of original image
        landmarks[0::2] = landmarks[0::2] * w/2 + w/2 + x
        landmarks[1::2] = landmarks[1::2] * h/2 + h/2 + y
        faces_keypoints.append(landmarks)
        
        # Paint keypoints on image
        for point in range(15):
            cv2.circle(image_copy, (landmarks[2*point], landmarks[2*point + 1]), 2, (255, 255, 0), -1)
        print(faces_keypoints)
    return image_copy, faces, faces_keypoints
img, faces, keypoints = face_keypoint_detector(image)

