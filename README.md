To train the model

Python3 train_original.py -f main_filter.txt

The main_filter.txt file contains the Xcall Ids of the images that you are giving to train the model. A csv file consisting of pixel values of pointed landmarks
should be given in the utils.py file. You can find the .csv file in this repository itself.