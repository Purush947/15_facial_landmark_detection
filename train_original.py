import numpy as np
import matplotlib.pyplot as plt
import math
import cv2                     # OpenCV library for computer vision
from PIL import Image
import time 

from utils import *


import numpy as np
from PIL import Image

import os
import argparse
import cv2
import csv


ap = argparse.ArgumentParser()
ap.add_argument("-i", "--image", type=str, default="",
	help="path to input image file")
ap.add_argument('-f','--file',type=str,default='',
	help="path to file")
args = vars(ap.parse_args())

images = args["image"]
file = args["file"]
my_list1 = []	
count = 0


# for each_file in file:
# 	print ("File = %s" %each_file)
# 	lines = [line.rstrip('\n') for line in open(each_file)]
# 	for line in lines:
# 		print("special")
# 		print(line)
# 		print("specil_2")



with open(file) as file:
	lines = file.readlines()
	for file_name in lines:
		file_name2=file_name.rstrip('\n')
		# print(file_name2)
		# print(type(file_name))
		# file_name = str(file_name) + ".png"
		# print(file_name)
		# source=src+'/'+file_name
		# print(source)
		# print(images)
		# print(source)

		# os.system ("mv"+ " " + source + " " + dst)

		images2 = os.path.join(images,file_name2)
		img_file = Image.open(images2) # imgfile.show()

		# Make image Greyscale
		img_grey = img_file.convert('L')
		pixels = list(img_grey.getdata())
		# print(pixels)
		pixel2 = " ".join([str(num) for num in pixels])
		# with open("moodoff.csv", 'a') as f:
		# 	fieldnames = ['x']
		# 	writer = csv.DictWriter(f, fieldnames=fieldnames)
		# 	writer.writerow({'x' :pixel2})
		# arr = [1,2,3,4,5]
		# print(pixel2)
		# print(pixels)
		# print(pixels)

		# print (str(pixels)[1:-1])
		my_list = str(pixels)[1:-1]
		my_list = my_list.split(",")
		# print(my_list)
		# my_str_as_bytes = str.encode(my_list)
		# # print(my_str_as_bytes)


		# df['Image'] = df['Image'].apply(lambda im: np.fromstring(im, sep=' '))
		pixels1 = np.fromstring(pixel2, sep=' ')
		print(type(pixels1))
		# print("dfimage_1")
		print(pixels1)
		my_list1.append(pixels1)
		
















# for fl in os.listdir(images):
# 	#print(fl)
# 	if fl == ".DS_Store" or fl == "_DS_Store":
# 		#print(fl)
# 		print("stupid files")

# 	else:
# 		images2 = os.path.join(images,fl)
# 		img_file = Image.open(images2) # imgfile.show()

# 		# Make image Greyscale
# 		img_grey = img_file.convert('L')
# 		pixels = list(img_grey.getdata())
# 		# print(pixels)

# 		# print (str(pixels)[1:-1])
# 		my_list = str(pixels)[1:-1]
# 		# my_str_as_bytes = str.encode(my_list)
# 		# # print(my_str_as_bytes)


# 		# df['Image'] = df['Image'].apply(lambda im: np.fromstring(im, sep=' '))
# 		pixels1 = np.fromstring(my_list, sep=',')
# 		print("dfimage_1")
# 		print(pixels1)
# 		my_list1.append(pixels1)
# print("dfimage_2")
# print(my_list1)
my_list1 = np.asarray(my_list1)
print(type(my_list1))
print(my_list1)
print("vstack")
X = np.vstack(my_list1) / 255.
print(X)
X = X.astype(np.float32)
# print(X)
print("reshaped")
X = X.reshape(-1, 96, 96, 1)
print(X)





# Load training set
X_train, y_train = X, load_data(my_list1)
print("X_train.shape == {}".format(X_train.shape))
print("y_train.shape == {}; y_train.min == {:.3f}; y_train.max == {:.3f}".format(
    y_train.shape, y_train.min(), y_train.max()))

# Load testing set
X_test, _ = load_data(test=True)
print("X_test.shape == {}".format(X_test.shape))

import sklearn

X_train, y_train = sklearn.utils.shuffle(X_train, y_train)

X_test, y_test = X_train[:500], y_train[:500]
X_train, y_train = X_train[500:], y_train[500:]



#Image Scaling and Rotation
image_number = 7
scale_factor = .9
rotation = 5 #In degrees

# Unnormalize the keypoints
keypoints = y_train[image_number] * 48 + 48

# Use openCV to get a rotation matrix
M = cv2.getRotationMatrix2D((48,48),15, .9)
dst = cv2.warpAffine(np.squeeze(X_train[image_number]),M,(96,96))
new_keypoints = np.zeros(30)
for i in range(15):
    coord_idx = 2*i
    print("print(coord_idx")
    print(coord_idx)
    old_coord = keypoints[coord_idx:coord_idx+2]
    print("print(old_coord")
    print(old_coord)
    new_coord = np.matmul(M,np.append(old_coord,1))
    new_keypoints[coord_idx] += new_coord[0]
    new_keypoints[coord_idx+1] += new_coord[1]

# Plot the image and the augmented image
fig = plt.figure(figsize=(12,12))
ax = fig.add_subplot(121)
ax.imshow(np.squeeze(X_train[image_number]), cmap='gray')
ax.scatter(keypoints[0::2], 
        keypoints[1::2], 
        marker='o', 
        c='c', 
        s=20)
ax2 = fig.add_subplot(122)
ax2.imshow(dst, cmap='gray')
ax2.scatter(new_keypoints[0::2], 
        new_keypoints[1::2], 
        marker='o', 
        c='c', 
        s=20)




def scale_rotate_transform(data, labels, rotation_range=10, scale_range=.1):
    '''
    Scales and rotates an image and the keypoints.
    '''
    aug_data = np.copy(data)
    aug_labels = np.copy(labels)
    
    # Apply rotation and scale transform
    for i in range(len(data)):
        # Unnormalize the keypoints
        aug_labels[i] = aug_labels[i]*48 + 48
        scale_factor = 1.0 + (np.random.uniform(-1,1)) * scale_range
        rotation_factor = (np.random.uniform(-1,1)) * rotation_range

        # Use openCV to get a rotation matrix
        M = cv2.getRotationMatrix2D((48,48), rotation_factor, scale_factor)
        aug_data[i] = np.expand_dims(cv2.warpAffine(np.squeeze(aug_data[i]),M,(96,96)), axis=2)
        for j in range(15):
            coord_idx = 2*j
            old_coord = aug_labels[i][coord_idx:coord_idx+2]
            new_coord = np.matmul(M,np.append(old_coord,1))
            aug_labels[i][coord_idx] = new_coord[0]
            aug_labels[i][coord_idx+1] = new_coord[1]
        #normalize aug_labels    
        aug_labels[i] = (aug_labels[i] - 48)/48
    
    return aug_data, aug_labels




def horizontal_flip(data, labels):
    '''
    Takes a image set and keypoint labels and flips them horizontally.
    '''
    
    # Flip the images horizontally
    flipped_data = np.copy(data)[:,:,::-1,:]
    flipped_labels = np.zeros(labels.shape)
    for i in range(data.shape[0]):
        # Flip the x coordinates of the key points
        flipped_labels[i] += labels[i]
        flipped_labels[i, 0::2] *= -1

        # Flip the indices of the left right keypoints
        flip_indices = [
                (0, 2), (1, 3),
                (4, 8), (5, 9), (6, 10), (7, 11),
                (12, 16), (13, 17), (14, 18), (15, 19),
                (22, 24), (23, 25),
                ]

        for a,b in flip_indices:
            flipped_labels[i,a], flipped_labels[i,b] = flipped_labels[i,b], flipped_labels[i,a]
    
    return flipped_data, flipped_labels



def data_augmentation(data, labels, rotation_range=10, scale_range=.1, h_flip=True):
    '''
        Takes in a the images and keypoints, applys a random rotation and scaling. Then flips the image
        and keypoints horizontally if specified.
    
    '''
    
    aug_data, aug_labels = scale_rotate_transform(data, labels, rotation_range, scale_range)
    if h_flip:
        aug_data, aug_labels = horizontal_flip(aug_data, aug_labels)
    
    return aug_data, aug_labels



data_hflip, labels_hflip = data_augmentation(X_train, y_train, 0.0, 0.0, True)


keypoints = y_train[image_number]*48 +48
new_keypoints = labels_hflip[image_number]*48+48

fig = plt.figure(figsize=(12,12))
ax = fig.add_subplot(121)
ax.imshow(np.squeeze(X_train[image_number]), cmap='gray')
ax.scatter(keypoints[0::2], 
        keypoints[1::2], 
        marker='o', 
        c='c', 
        s=20)
ax2 = fig.add_subplot(122)
ax2.imshow(np.squeeze(data_hflip[image_number]), cmap='gray')
ax2.scatter(new_keypoints[0::2], 
        new_keypoints[1::2], 
        marker='o', 
        c='c', 
        s=20)


X_aug = np.concatenate((X_train, data_hflip), axis=0)
y_aug = np.concatenate((y_train, labels_hflip), axis=0)



X_train_transformed, y_train_transformed = data_augmentation(X_aug, y_aug, 15.0, .1, False)
X_train_transformed2, y_train_transformed2 = data_augmentation(X_aug, y_aug, 15.0, .1, False)


fig = plt.figure(figsize=(20,20))
fig.subplots_adjust(left=0, right=1, bottom=0, top=1, hspace=0.05, wspace=0.05)
for i in range(9):
    ax = fig.add_subplot(3, 3, i + 1, xticks=[], yticks=[])
    plot_data(X_train_transformed[i], y_train_transformed[i], ax)


X_aug = np.concatenate((X_aug, X_train_transformed, X_train_transformed2), axis=0)
y_aug = np.concatenate((y_aug, y_train_transformed, y_train_transformed2), axis=0)


X_aug.shape


X_train_transformed, y_train_transformed = data_augmentation(X_aug, y_aug, 15.0, .1, False)
X_train_transformed2, y_train_transformed2 = data_augmentation(X_aug, y_aug, 15.0, .1, False)



X_aug = np.concatenate((X_aug, X_train_transformed, X_train_transformed2), axis=0)
y_aug = np.concatenate((y_aug, y_train_transformed, y_train_transformed2), axis=0)


X_aug.shape


# Import deep learning resources from Keras
from keras.models import Sequential
from keras.layers import Conv2D, MaxPooling2D, Dropout, GlobalAveragePooling2D, BatchNormalization
from keras.layers import Flatten, Dense


## TODO: Specify a CNN architecture
# Your model should accept 96x96 pixel graysale images in
# It should have a fully-connected output layer with 30 values (2 for each facial keypoint)

model = Sequential()
# model = load_model("P_my_model.h5")
# Conv layer1
model.add(Conv2D(32, 3, strides=(1,1), padding='same', activation='elu', input_shape=(96,96,1)))
model.add(BatchNormalization())
model.add(Dropout(.2))
model.add(MaxPooling2D((2,2), strides= 2, padding='same'))

# Conv layer2
model.add(Conv2D(64, 3, strides=(1,1), padding='same', activation='elu'))
model.add(BatchNormalization())
model.add(Dropout(.2))
model.add(MaxPooling2D((2,2), strides= 2, padding='same'))

# Conv layer3
model.add(Conv2D(128, 3, strides=(1,1), padding='same', activation='elu'))
model.add(BatchNormalization())
model.add(Dropout(.2))
model.add(MaxPooling2D((2,2), strides= 2, padding='same'))

# Conv layer4
model.add(Conv2D(256, 3, strides=(1,1), padding='same', activation='elu'))
model.add(BatchNormalization())
model.add(MaxPooling2D((2,2), strides= 2, padding='same'))

# Conv layer5
model.add(Conv2D(256, 3, strides=(1,1), padding='same', activation='elu'))
model.add(BatchNormalization())
model.add(MaxPooling2D((2,2), strides= 2, padding='same'))

# Conv layer6
model.add(Conv2D(512, 3, strides=(1,1), padding='same', activation='elu'))
model.add(BatchNormalization())
model.add(MaxPooling2D((2,2), strides= 2, padding='same'))

#Flatten Layer
model.add(GlobalAveragePooling2D())
model.add(BatchNormalization())

#Fully Connected Layer 2
# model.add(Dense(108, activation='relu'))
model.add(Dense(30, activation='elu'))



# Summarize the model
model.summary()




from keras.optimizers import SGD, RMSprop, Adagrad, Adadelta, Adam, Adamax, Nadam
from keras.callbacks import ReduceLROnPlateau

learning_rates = [.001, 0.0001, 0.00001]
batch_sizes = [32, 64, 128]
full_loss_hist = []
full_val_loss =[]

# from keras.callbacks import LearningRateScheduler

# def scheduler(epoch):
#     if epoch%2==0 and epoch!=0:
#         lr = K.get_value(model.optimizer.lr)
#         K.set_value(model.optimizer.lr, lr*.9)
#         print("lr changed to {}".format(lr*.9))
#     return K.get_value(model.optimizer.lr)

# lr_decay = LearningRateScheduler(scheduler)

# model.fit_generator(train_gen, (nb_train_samples//batch_size)*batch_size,
#                   nb_epoch=100, verbose=1,
#                   validation_data=valid_gen,    nb_val_samples=val_size,
#                   callbacks=[lr_decay])


# reduce_lr = ReduceLROnPlateau(monitor='val_loss', factor=0.2,
#                               patience=5, min_lr=0.001)
# model.fit(X_train, Y_train, callbacks=[reduce_lr])


# <Incremental leraning rate>
    ## TODO: Compile the model
# model.compile(optimizer='adam',loss='mse')
# reduce_lr = ReduceLROnPlateau(monitor='val_loss', factor=0.2, patience=5, min_lr=0.001)
# # model.compile(optimizer=Adam(lr = reduce_lr), loss='mean_squared_error')

# # for batch_size in batch_sizes:
# 	# print('learning rate: {}, batch size: {}'.format(lr,batch_size))
# hist = model.fit(X_aug, y_aug, callbacks=[reduce_lr], batch_size=batch_size, epochs=500, validation_data=(X_test, y_test), verbose=2)

# ## TODO: Train the model
# # hist = model.fit(X_aug, y_aug, batch_size=batch_size, epochs=30, validation_data=(X_test, y_test), verbose=2)
# full_loss_hist.append(hist.history['loss'])
# full_val_loss.append(hist.history['val_loss'])




for lr in learning_rates:
	for batch_size in batch_sizes:
		model.compile(optimizer=Adam(lr=lr), loss='mean_squared_error')
		print('learning rate: {}, batch size: {}'.format(lr,batch_size))
		# hist = model.fit(X_aug, y_aug, callbacks=[reduce_lr], batch_size=batch_size, epochs=500, validation_data=(X_test, y_test), verbose=2)

		# TODO: Train the model
		hist = model.fit(X_aug, y_aug, batch_size=batch_size, epochs=30, validation_data=(X_test, y_test), verbose=2)
		full_loss_hist.append(hist.history['loss'])
		full_val_loss.append(hist.history['val_loss'])


# TODO: Save the model as model.h5
model.save('With_original.h5')